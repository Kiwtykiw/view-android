package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SecondMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_main);
    }

    public void linkWeb(View v){
        String myUriString = "https://fairyanime.com/fairy-tail/";
        Intent myActivity2 = new Intent(Intent.ACTION_VIEW,
                Uri.parse(myUriString));
        startActivity(myActivity2);

    }


    public  void Back1 (View v){
        Intent intent2 = new Intent(SecondMainActivity.this,MainActivity.class);
        startActivity(intent2);
    }
}